const gulp = require('gulp');
const concat=require('gulp-concat');
const uglify = require('gulp-uglify-es').default;


//Gulp JS task: bundle and minify all the javascripts in /js -folder to bundle.min.js
gulp.task('js', function(){
    return gulp.src(['_src/js/config.js','_src/js/index.js'])
        .pipe(concat('ubf_cookieconsent.min.js'))
        .pipe(uglify())
        .pipe(gulp.dest('dist/'));
});
gulp.task('default', gulp.series('js'));