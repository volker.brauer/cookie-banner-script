# cookie-banner-script

# For Development:
Clone reporitory and install via npm:
npm install

Check examples: You can see the different 3 variants in examples folder (must be served over http protocol to work) 

Check _src-folder: There are the config settings in the config.js (including the content of the banner in german and english)
Basically, we have 3 Banner Variants: 1: The Modal Popup banner, 2: The fixed Bottom Banner, 3: randomizes Variant 1 or 2 randomly (50% probability)
The Banner supports 2 languages: 'de' and 'en'. 
To use your own language cookie, you can set the value and the cookie name here. 

The Banner Markup, Cookie  functions etc. are in index.js main file - you probably do not have to make any changes here.

After Changes, create new minified productive js-Version via gulp-command:
gulp

# For production: 
Add dist/ubf_cookieconsent.min.js to your website