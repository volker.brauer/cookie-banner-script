/*********
 * 
 * VB - 24.04.2023: Cookie Consent Banner Script
 * It checks, if a Consent Cookie is already set. If no cookie is set, it displays either a Popup/Modal Banner (Variant 1) or a fixed-Bottom Version (Variant 2) or Variant3: randomly Variant 1 or 2 (for A/B-Testing purposes)
 * In config.js, there are some configuration options for the script to embed, cookie expiry days, the cookie name of the language cookie, the content of the banner etc. 

**********/

//Function to create and display the Cookie Modal Banner
function createModalBanner() {
    let modal = document.createElement('div');
    modal.setAttribute("id", "cookieBanner");
    modal.setAttribute("role","region");
    modal.setAttribute("aria-label","Cookie Banner");
    modal.setAttribute("class", "cookieBanner cookieBanner--modal");
    modal.setAttribute("style", 'position: fixed; top:0;bottom: 0;right: 0;left: 0;background-color: rgba(0,0,0,.6);z-index: 10000;display: flex;align-items: center;justify-content: center;max-height:100%;max-width:100%;overflow-y: auto;overflow-x: hidden;');

    //Mobile adjustments variables
    let mobileFlexDirection = 'flex-direction: row';
    let banner_margin = '5% 25%';
    if (window.innerWidth < 1400) {
        banner_margin = '0% 3%';
        //   mobileFlexDirection = 'flex-direction: column';
    }
    //Get Markup for BannerInnerContent in the needed language
    let lang = checkCookie('lang');
    let buttonTexts = createBannerContentMarkup('buttonTexts', lang);
    let buttonTextConfirm = buttonTexts.allow;
    let buttonTextDeny = buttonTexts.deny;
    let bannerHeadline = createBannerContentMarkup('headline', lang);
    let bannerParagraphs = createBannerContentMarkup('paragraphs', lang);
    let bannerFooterLinks = createBannerContentMarkup('footerLinks', lang);
    let bannerContent = `
        <div id="bannerContent">
            ${bannerHeadline}
            ${bannerParagraphs}
            <button id="cookieModalBanner__confirm" class="cookieBanner__button" onclick="setCookie('confirm');" style="width: 100%; display: block; background-color:${config.buttonsBackGroundColor};  margin: 1.5rem 0rem; padding: 1rem; border: none; font-weight: bold;">${buttonTextConfirm}</button>
            <button id="cookieModalBanner__deny" class="cookieBanner__button" onclick="setCookie('deny');"  style="width: 100%; display: block; background-color:${config.buttonsBackGroundColor};  margin: 1rem 0rem; padding: 1rem; border: none; font-weight: bold;">${buttonTextDeny}</button>
            <ul style='list-style: none; display: flex; align-items: center; justify-content: center; padding: 1rem;${mobileFlexDirection}'>
                ${bannerFooterLinks}
            </ul>
        </div>`;

    let banner = `<div id='ubfBanner' style="margin: ${banner_margin};padding: 2rem;background-color: #fff;overflow-y: auto;position: absolute;top:1rem;">${bannerContent}</div>`;
    modal.innerHTML = banner;
    document.querySelector('body').append(modal);
    return true;
}

//VB - 24.04.2023: Function to create and display the Cookie Bottom Banner
function createBottomBanner() {
    let banner = document.createElement('div');
    banner.setAttribute("style", 'position: fixed; bottom: 0;right: 0;left: 0;background-color: rgba(240,240,240,1);z-index: 10000;width:100%; overflow-y: auto;');
    banner.setAttribute("id", "cookieBanner");
    banner.setAttribute("class", "cookieBanner cookieBanner--bottom");
    banner.setAttribute("role", "region");
    banner.setAttribute("aria-label", "Cookie Banner");
    //Mobile Adjustment variables
    let mobileFlexDirection = "flex-direction: row;";
    let buttonsFlexDirection = "flex-direction: row-reverse;";
    let bannerContentCol__firstColumnWidth = 'width: 50%;';
    let bannerContentCol__secondColumnWidth = 'width: 50%;';

    if (window.innerWidth < 1320) {
        mobileFlexDirection = "flex-direction: column;";
        buttonsFlexDirection = "flex-direction: column;";
        bannerContentCol__firstColumnWidth = "width:100%;";
        bannerContentCol__secondColumnWidth = "width:100%;";
    }
    //Get Markup for BannerInnerContent in the needed language
    let lang = checkCookie('lang');
    let buttonTexts = createBannerContentMarkup('buttonTexts', lang);
    let buttonTextConfirm = buttonTexts.allow;
    let buttonTextDeny = buttonTexts.deny;
    //let bannerHeadline=createBannerContentMarkup('headline',lang);
    let bannerParagraphs = createBannerContentMarkup('paragraphs', lang);
    let bannerFooterLinks = createBannerContentMarkup('footerLinks', lang);
    let bannerContent = `
        <div id="bannerContentCol__first" style="${bannerContentCol__firstColumnWidth} padding: 12px">
           ${bannerParagraphs}
        </div>
        <div id="bannerContentCol__second" style="${bannerContentCol__secondColumnWidth} padding: 12px;display: flex;flex-direction: column; justify-content: center;">  
            <div id="bannerContent__buttons" style="display: flex; ${buttonsFlexDirection}; align-items: center;">
                <button id="cookieBottomBanner__confirm" class="cookieBanner__button" onclick="setCookie('confirm');"  style="width: 100%; background-color:${config.buttonsBackGroundColor}; margin: 12px; padding: 1rem; border: none; font-weight: bold;">${buttonTextConfirm}</button>
                <button id="cookieBottomBanner__deny" class="cookieBanner__button" onclick="setCookie('deny');"   style="width: 100%; background-color:${config.buttonsBackGroundColor};  margin: 12px; padding: 1rem; border: none; font-weight: bold;">${buttonTextDeny}</button>
            </div>
            <ul style='list-style: none; display: flex; align-items: center; justify-content: center; padding: .5rem; margin-bottom: 0;flex-direction: row;'>
                ${bannerFooterLinks}
            </ul>
         </div>`;
    let bannerContentWrapper = `<div id="bannerContentWrapper" style="padding: 12px 5%;display: flex;align-items: center; overflow-y: auto; ${mobileFlexDirection}">${bannerContent}</div>`;
    banner.innerHTML = bannerContentWrapper;
    document.querySelector('body').append(banner);
    //Adjustment for very small displays: make banner scrollable if the banner is higher than the viewport
    if (window.innerHeight < banner.offsetHeight) {
        banner.style.height = '100%';
        document.getElementById('bannerContentWrapper').style.height = '100%';
    }
    return true;
}

//Creates the Banner Inner Content as defined in config.js (headline, footerlinks, paragraphs) in german or english (if a language Cookie is set to english) 
function createBannerContentMarkup(variant, language) {
    let lang = language;
    let markup = '';
    if (variant == 'footerLinks') {
        if (lang == 'de') {
            let footerLinks = config.bannerContent.de.footerLinks;
            footerLinks.forEach((footerLink) => {
                markup = markup + '<li style="padding: 0 8px; color: #000;"><a style="color: #000; font-size: 14px; text-decoration: underline;" target="_BLANK" href="' + footerLink.link + '">' + footerLink.label + '</a></li>';
            });
        } else if (lang == 'en') {
            let footerLinks = config.bannerContent.en.footerLinks;
            footerLinks.forEach((footerLink) => {
                markup = markup + '<li style="padding: 0 8px; color: #000;"><a style="color: #000; font-size: 14px; text-decoration: underline;" target="_BLANK" href="' + footerLink.link + '">' + footerLink.label + '</a></li>';
            });
        }
    }
    else if (variant == 'paragraphs') {
        if (lang == 'de') {
            let paragraphs = config.bannerContent.de.paragraphs;
            paragraphs.forEach((paragraph) => {
                markup = markup + '<p style="font-size: 16px">' + paragraph + '</p>';
            });
        } else if (lang == 'en') {
            let paragraphs = config.bannerContent.en.paragraphs;
            paragraphs.forEach((paragraph) => {
                markup = markup + '<p style="font-size: 16px">' + paragraph + '</p>';
            });
        }
    } else if (variant == 'headline') {
        if (lang == 'de') {
            markup = '<h2 style="text-align: center; padding: 1rem;font-weight: bold; font-size: 24px">' + config.bannerContent.de.headline + "</h2>";
        } else if (lang == 'en') {
            markup = '<h2 style="text-align: center; padding: 1rem;font-weight: bold; font-size: 24px">' + config.bannerContent.en.headline + "</h2>";
        }
    } else if (variant == 'buttonTexts') {
        if (lang == 'de') {
            markup = config.bannerContent.de.buttonTexts;
        } else if (lang == 'en') {
            markup = config.bannerContent.en.buttonTexts;
        }
    }
    return markup;
}

//Function which is executed on page load
//First, check if there is already the consent cookie. If there is no, render the configured Banner Variant
//If the consent tracking cookie exists and is set to true, load the Tracking script. If a consent cookie exists and set to false, do nothing (not load tracking script, not load banner).
//If a censent cookie exists and set to 'confirm', do not display banner, but load Tracking Scripts
function initializeBanner() {
    let hasCookie = checkCookie('consent');
    //Create Banner Variant, if no cookie is set AND if the page is not on the doNotShow-Pages (config.doNotShow)
    let url = window.location.href;
    let isActivePage = true;
    config.doNotShowPages.forEach((page) => {
        if (url.indexOf(page) != -1) {
            isActivePage = false;
        }
    });
    if (hasCookie == false && isActivePage) {
        //Check Variant in Config: 1: Modal Popup Banner, 2: Bottom Banner, 3: randomize 50% BottomBanner,50% ModalBanner
        if (config.bannerVariant == 1) {
            createModalBanner();
        } else if (config.bannerVariant == 2) {
            createBottomBanner();
        } else if (config.bannerVariant == 3) {
            //Random Number between 0-9
            let number = Math.floor(Math.random() * 10);
            if (number % 2 == 0) {
                createBottomBanner();
            } else {
                createModalBanner();
            }

        } else {
            return false;
        }
    }
    //If there is a Cookie set to confirm, load Tracking Script 
    else if (hasCookie == 'confirm') {
        loadTrackingScripts();
    }
    //Return false otherwise-> Tracking script does not get loaded, no banner gets displayed
    else {
        return false;
    }
}

//Sets a Cookie name ubf-cookie-consent with the expiry times in config.js and a cookie value true/false. 
//@param: variant: 'confirm' or 'deny' (handler-function for the allow/deny-buttons)
//Handler Function by the allow/deny-buttons in the banner. Param: variant with 'confirm' (by clicking confirm-button)->Coookie value true
// or 'deny' by clicking deny-button -> Cookie Value false
function setCookie(variant) {
    //Remove Banner, if it exists
    let banner = document.querySelector('.cookieBanner');
    if (banner != null) {
        banner.remove();
    }

    //Cookie variables: Expiry days and cookie value (true/false)
    let expiryDays = '';
    let cookieValue = '';
    if (variant == 'confirm') {
        //Set Confirm Cookie Values
        cookieValue = true;
        expiryDays = config.cookieAllowExpiryDays;
        loadTrackingScripts();

    } else if (variant == "deny") {
        //Set deny cookie Values 
        cookieValue = false;
        expiryDays = config.cookieDenyExpiryDays;
    }
    //Create Cookie
    const d = new Date();
    d.setTime(d.getTime() + (expiryDays * 24 * 60 * 60 * 1000));
    let expirydate = "expires=" + d.toUTCString();
    document.cookie = "ubf-cookie-consent" + "=" + cookieValue + ";" + expirydate + ";path=/;domain=" + config.domain;
    return true;
}

//Function to Check the Browser Cookies:
//@Param: variant: 'lang' or 'consent'
//Variant "consent": Check, if a Consent Cookie is set. If No Cookie is set, return false. 
//If a Consent Cookie is set, return either "confirm" (if the cookievalue is true) or "deny" (if Cookie Value is false)
//Variant "lang": Check, if a Browser language cookie (as defined in config.js) is set to english language
function checkCookie(variant) {
    let cookies = decodeURIComponent(document.cookie);
    cookies = cookies.split(';');
    //Check, if a language cookie is set
    if (variant == 'lang') {
        let lang = config.defaultLanguage;
        cookies.forEach((cookie) => {
            let cookie_array = cookie.split('=');
            let cookieName = cookie_array[0].trim();
            let cookieValue = cookie_array[1];
            //If there is a language cookie set to english (as defined in config) or page is set to english via Get-Parameter (as in Wordpress Blogs via WPML), set lang to english
            if (cookieName == config.languageCookieName && cookieValue == config.languageCookieValue_en || window.location.href.indexOf('?lang=en') !== -1) {
                lang = 'en';
            }
        });
        return lang;
    } else if (variant == 'consent') {
        let cookieValue = false;
        cookies.forEach((cookie) => {
            if (cookie.indexOf('ubf-cookie-consent') !== -1) {
                if (cookie.indexOf('true') != -1) {
                    cookieValue = "confirm";
                } else if (cookie.indexOf('false') != -1) {
                    cookieValue = "deny";
                }
            }
        });
        //Check if there is already a Cookie: If there is a cookie,return either confirm (if value is true) or deny (if value is false)
        return cookieValue;
    }
}

//Function to load the tracking scripts (append script to body)
function loadTrackingScripts() {
    config.scriptsUrl.forEach((scriptItem) => {
        let script = document.createElement("script");
        script.src = scriptItem;
        script.defer = true;
        document.querySelector('body').appendChild(script);
    });
    return true;
}

//Functions for checking the checkbox on data privacy page - not directly used in the cookie banner
//On Page Load: Check, if a Consent Cookie is set -> If a Cookie Consent exists and has value 'confirm' -> Activate Checkbox (selector: #ubfCookieCheckbox) 
function checkCookieCheckBox() {
    let hasAcceptCookie = checkCookie('consent');
    if (hasAcceptCookie == "confirm") {
        document.querySelector('#ubfCookieCheckbox').setAttribute("checked", "true");
    }
}

//On Checkbox change: setCookie ("allow" or "deny")
function setCheckBoxCookie(checkbox) {
    if (checkbox.checked) {
        setCookie('confirm');
    } else {
        setCookie('deny');
    }
}

window.addEventListener("DOMContentLoaded", () => {
    initializeBanner();
});