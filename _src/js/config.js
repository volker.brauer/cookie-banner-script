const config = {
    domain: 'uni-bielefeld.de',
    scriptsUrl: ['https://siteimproveanalytics.com/js/siteanalyze_1983.js'],//Scripts to embed on permission
    bannerVariant: 1, //The Banner variant can be 1 (the Modal Popup Banner), 2 (the Banner fixed on the bottom) or 3 (mixed: 50% variant 1, 50% variant 2)
    doNotShowPages:['/impressum/','/datenschutzhinweise/','/barrierefreiheit/','/datenschutz'], //For legal reasons, Cookie Banner should not get displayed on legal pages
    cookieAllowExpiryDays: 180, //Expire days, if allow Cookie is set
    cookieDenyExpiryDays: 180, //Expire days, if deny cooky is set,
    defaultLanguage:'de', //Default language, can be 'de' or 'en'
    languageCookieName: 'RoxenConfig', //Name of the Cookie which defines the language. Default is the Roxen language Cookie
    languageCookieValue_en: 'en', //The value of the language Cookie, if it is set to english
    buttonsBackGroundColor:'#14f5b4', //Background Color of the Buttons, Default to Uni digital green color
    //Banner Content
    bannerContent: {
        de: {
            headline: "Datenschutzeinstellung",
            paragraphs: [
              //  'Diese Website verwendet Cookies, um eine bestmögliche Nutzung der Webseite zu ermöglichen. Einige dieser Cookies sind essenziell und sorgen für die technische Zugänglichkeit und Nutzbarkeit der Webseite. Sie gewährleisten wesentliche Grundfunktionalitäten, wie die Navigation auf der Webseite, die korrekte Darstellung im Internetbrowser und die Abfrage der Zustimmung. Ohne diese essenziellen Cookies wäre die Webseite nicht funktionsfähig..',
              //  'Darüber hinaus werden auf dieser Webseite optionale Cookies für die Webseitenanalyse mit der Software Siteimprove eingesetzt. Die dabei durchgeführte Nutzungsanalyse erfolgt anonymisiert und ermöglicht keine Rückschlüsse auf individuelle Besucher.',
              //  'Einige Cookies sind für den Besuch dieser Webseite notwendig, also essenziell. Ohne diese Cookies wäre Ihr Endgerät ansonsten zum Beispiel nicht in der Lage, sich Ihre Datenschutzauswahl zu merken.',
              //  'Durch Klicken auf "Alle akzeptieren" wird die Einwilligung zur Nutzung aller Cookies erteilt. Die Einwilligung zur Verwendung von Cookies kann jederzeit unter <a href="https://www.uni-bielefeld.de/datenschutzhinweise/" target="_SELF" style="text-decoration: underline; color: #000;">Datenschutzerklärung</a> eingesehen, verwaltet und mit der Wirkung für die Zukunft widerrufen werden. Dort finden sich auch zusätzliche Informationen zu den verwendeten Cookies und weiteren Technologien.',
              'Diese Webseite verwendet Cookies und ähnliche Technologien. Einige davon sind essentiell, um die Funktionalität der Website zu gewährleisten, während andere uns helfen, die Website und Ihre Erfahrung zu verbessern. Falls Sie zustimmen, verwenden wir Cookies und Daten auch, um Ihre Interaktionen mit unserer Webseite zu messen. Sie können Ihre Einwilligung jederzeit unter <a href="https://www.uni-bielefeld.de/datenschutzhinweise/" target="_SELF" style="text-decoration: underline; color: #000;">Datenschutzerklärung</a> einsehen und mit der Wirkung für die Zukunft widerrufen. Auf der Seite finden Sie auch zusätzliche Informationen zu den verwendeten Cookies und Technologien.'
            ],
            buttonTexts: {
                allow: 'Alle akzeptieren',
                deny: 'Nur notwendige Cookies zulassen',
            },
            footerLinks: [ //The Links in the Footer of the Banner (label and url)
                {
                    label: 'Impressum',
                    link: 'https://www.uni-bielefeld.de/impressum',
                },
                {
                    label: 'Datenschutz',
                    link: 'https://www.uni-bielefeld.de/datenschutzhinweise/',
                },
                {
                    label: 'Barrierefreiheit',
                    link: 'https://www.uni-bielefeld.de/barrierefreiheit/',
                }
            ],
        },
        en: {
            headline: "Privacy Policy",
            paragraphs: [
                'This website uses cookies and similar technologies. Some of these are essential to ensure the functionality of the website, while others help us to improve the website and your experience. If you consent, we also use cookies and data to measure your interactions with our website. You can view and withdraw your consent at any time with future effect at <a href="https://www.uni-bielefeld.de/datenschutzhinweise/" target="_SELF" style="text-decoration: underline; color: #000;"> our Privacy Policy site</a>. Here you will also find additional information about the cookies and technologies used.'
            ],
            buttonTexts: {
                allow: 'Accept all',
                deny: 'Accept only essential cookies',
            },
            footerLinks: [ //The Links in the Footer of the Banner (label and url)
                {
                    label: 'Imprint',
                    link: 'https://www.uni-bielefeld.de/impressum',
                },
                {
                    label: 'Privacy Policy',
                    link: 'https://www.uni-bielefeld.de/datenschutzhinweise/',
                },
                {
                    label: 'Accessibility',
                    link: 'https://www.uni-bielefeld.de/barrierefreiheit/',
                },
            ],
        }
    }
}
